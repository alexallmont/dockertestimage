FROM alpine:3.7

RUN apk update && apk upgrade \
  && apk add --update cmake alpine-sdk vim qt5-qtbase qt5-qtmultimedia-dev qt5-qttools-dev \
  && rm -rf /var/cache/apk/*